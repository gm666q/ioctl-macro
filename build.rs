use std::env;

fn main() {
	let env = env::var("CARGO_CFG_TARGET_ENV").unwrap();
	let os = env::var("CARGO_CFG_TARGET_OS").unwrap();
	let ioctl = match os.as_ref() {
		"android" => "c_int",
		"linux" => match env.as_ref() {
			"gnu" => "c_ulong",
			"musl" => "c_int",
			"uclibc" => "c_ulong",
			_ => "",
		},
		_ => "",
	};
	println!("cargo:rustc-cfg=ioctl={:?}", ioctl);
}
