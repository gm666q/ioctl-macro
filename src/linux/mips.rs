#[macro_export]
macro_rules! _IOC_SIZEBITS {
	() => {
		13
	};
}
#[macro_export]
macro_rules! _IOC_DIRBITS {
	() => {
		3
	};
}

#[macro_export]
macro_rules! _IOC_NONE {
	() => {
		1
	};
}
#[macro_export]
macro_rules! _IOC_READ {
	() => {
		2
	};
}
#[macro_export]
macro_rules! _IOC_WRITE {
	() => {
		4
	};
}
