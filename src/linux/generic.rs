#[macro_export]
macro_rules! _IOC_SIZEBITS {
	() => {
		14
	};
}
#[macro_export]
macro_rules! _IOC_DIRBITS {
	() => {
		2
	};
}

#[macro_export]
macro_rules! _IOC_NONE {
	() => {
		0
	};
}
#[macro_export]
macro_rules! _IOC_WRITE {
	() => {
		1
	};
}
#[macro_export]
macro_rules! _IOC_READ {
	() => {
		2
	};
}
