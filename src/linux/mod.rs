#![allow(non_snake_case)]

#[cfg(feature = "extern_fn")]
use core::ffi::c_int;
#[cfg(all(feature = "extern_fn", ioctl = "c_ulong"))]
use core::ffi::c_ulong;

#[cfg(any(
	target_arch = "aarch64",
	target_arch = "arm",
	target_arch = "riscv32",
	target_arch = "riscv64",
	target_arch = "s390x",
	target_arch = "x86",
	target_arch = "x86_64",
))]
mod generic;
#[cfg(any(target_arch = "mips", target_arch = "mips64"))]
mod mips;
#[cfg(any(target_arch = "powerpc", target_arch = "powerpc64"))]
mod powerpc;
#[cfg(any(target_arch = "sparc", target_arch = "sparc64"))]
mod sparc;

#[cfg(feature = "extern_fn")]
extern "C" {
	#[cfg(ioctl = "c_int")]
	pub fn ioctl(fd: c_int, request: c_int, ...) -> c_int;
	#[cfg(ioctl = "c_ulong")]
	pub fn ioctl(fd: c_int, request: c_ulong, ...) -> c_int;
}

#[macro_export]
macro_rules! _IOC_NRBITS {
	() => {
		8
	};
}
#[macro_export]
macro_rules! _IOC_TYPEBITS {
	() => {
		8
	};
}

#[macro_export]
macro_rules! _IOC_NRMASK {
	() => {
		((1 << $crate::_IOC_NRBITS!()) - 1)
	};
}
#[macro_export]
macro_rules! _IOC_TYPEMASK {
	() => {
		((1 << $crate::_IOC_TYPEBITS!()) - 1)
	};
}
#[macro_export]
macro_rules! _IOC_SIZEMASK {
	() => {
		((1 << $crate::_IOC_SIZEBITS!()) - 1)
	};
}
#[macro_export]
macro_rules! _IOC_DIRMASK {
	() => {
		((1 << $crate::_IOC_DIRBITS!()) - 1)
	};
}

#[macro_export]
macro_rules! _IOC_NRSHIFT {
	() => {
		0
	};
}
#[macro_export]
macro_rules! _IOC_TYPESHIFT {
	() => {
		($crate::_IOC_NRSHIFT!() + $crate::_IOC_NRBITS!())
	};
}
#[macro_export]
macro_rules! _IOC_SIZESHIFT {
	() => {
		($crate::_IOC_TYPESHIFT!() + $crate::_IOC_TYPEBITS!())
	};
}
#[macro_export]
macro_rules! _IOC_DIRSHIFT {
	() => {
		($crate::_IOC_SIZESHIFT!() + $crate::_IOC_SIZEBITS!())
	};
}

#[cfg(ioctl = "c_int")]
#[macro_export]
macro_rules! _IOC {
	($dir:expr, $type:expr, $nr:expr, $size:expr) => {
		((($dir as core::ffi::c_int) << $crate::_IOC_DIRSHIFT!())
			| (($type as core::ffi::c_int) << $crate::_IOC_TYPESHIFT!())
			| (($nr as core::ffi::c_int) << $crate::_IOC_NRSHIFT!())
			| (($size as core::ffi::c_int) << $crate::_IOC_SIZESHIFT!()))
	};
}
#[cfg(ioctl = "c_ulong")]
#[macro_export]
macro_rules! _IOC {
	($dir:expr, $type:expr, $nr:expr, $size:expr) => {
		((($dir as core::ffi::c_ulong) << $crate::_IOC_DIRSHIFT!())
			| (($type as core::ffi::c_ulong) << $crate::_IOC_TYPESHIFT!())
			| (($nr as core::ffi::c_ulong) << $crate::_IOC_NRSHIFT!())
			| (($size as core::ffi::c_ulong) << $crate::_IOC_SIZESHIFT!()))
	};
}

#[macro_export]
macro_rules! _IOC_TYPECHECK {
	($t:ty) => {
		(core::mem::size_of::<$t>())
	};
}

#[macro_export]
macro_rules! _IO {
	($type:expr, $nr:expr, $size:ty) => {
		$crate::_IOC!($crate::_IOC_NONE!(), ($type), ($nr), 0)
	};
}
#[macro_export]
macro_rules! _IOR {
	($type:expr, $nr:expr, $size:ty) => {
		$crate::_IOC!($crate::_IOC_READ!(), ($type), ($nr), ($crate::_IOC_TYPECHECK!($size)))
	};
}
#[macro_export]
macro_rules! _IOW {
	($type:expr, $nr:expr, $size:ty) => {
		$crate::_IOC!($crate::_IOC_WRITE!(), ($type), ($nr), ($crate::_IOC_TYPECHECK!($size)))
	};
}
#[macro_export]
macro_rules! _IOWR {
	($type:expr, $nr:expr, $size:ty) => {
		$crate::_IOC!(
			$crate::_IOC_WRITE!() | $crate::_IOC_READ!(),
			($type),
			($nr),
			($crate::_IOC_TYPECHECK!($size))
		)
	};
}
#[macro_export]
macro_rules! _IOR_BAD {
	($type:expr, $nr:expr, $size:ty) => {
		$crate::_IOC!($crate::_IOC_READ!(), ($type), ($nr), core::mem::sizeof::<$size>())
	};
}
#[macro_export]
macro_rules! _IOW_BAD {
	($type:expr, $nr:expr, $size:ty) => {
		$crate::_IOC!($crate::_IOC_WRITE!(), ($type), ($nr), core::mem::sizeof::<$size>())
	};
}
#[macro_export]
macro_rules! _IOWR_BAD {
	($type:expr, $nr:expr, $size:ty) => {
		$crate::_IOC!(
			$crate::_IOC_WRITE!() | $crate::_IOC_READ!(),
			($type),
			($nr),
			core::mem::sizeof::<$size>()
		)
	};
}

#[macro_export]
macro_rules! _IOC_DIR {
	($nr:expr) => {
		((($nr) >> $crate::_IOC_DIRSHIFT!()) & $crate::_IOC_DIRMASK!())
	};
}
#[macro_export]
macro_rules! _IOC_TYPE {
	($nr:expr) => {
		((($nr) >> $crate::_IOC_TYPESHIFT!()) & $crate::_IOC_TYPEMASK!())
	};
}
#[macro_export]
macro_rules! _IOC_NR {
	($nr:expr) => {
		((($nr) >> $crate::_IOC_NRSHIFT!()) & $crate::_IOC_NRMASK!())
	};
}
#[macro_export]
macro_rules! _IOC_SIZE {
	($nr:expr) => {
		((($nr) >> $crate::_IOC_SIZESHIFT!()) & $crate::_IOC_SIZEMASK!())
	};
}

#[macro_export]
macro_rules! IOC_IN {
	() => {
		($crate::_IOC_WRITE!() << $crate::_IOC_DIRSHIFT!())
	};
}
#[macro_export]
macro_rules! IOC_OUT {
	() => {
		($crate::_IOC_READ!() << $crate::_IOC_DIRSHIFT!())
	};
}
#[macro_export]
macro_rules! IOC_INOUT {
	() => {
		(($crate::_IOC_WRITE!() | $crate::_IOC_READ!()) << $crate::_IOC_DIRSHIFT!())
	};
}
#[macro_export]
macro_rules! IOCSIZE_MASK {
	() => {
		($crate::_IOC_SIZEMASK!() << $crate::_IOC_SIZESHIFT!())
	};
}
#[macro_export]
macro_rules! IOCSIZE_SHIFT {
	() => {
		($crate::_IOC_SIZESHIFT!())
	};
}
