#![no_std]

#[cfg(any(target_os = "android", target_os = "linux"))]
pub use linux::*;

#[cfg(any(target_os = "android", target_os = "linux"))]
mod linux;
